# Release History

## 1.0.0-beta.3 (Unreleased)


## 1.0.0-beta.2 (2020-07-16)

- `GsonJsonSerializer` implements `JsonSerializer` instead of `ObjectSerializer`.
- Added implementations for `JsonNode` and its subclasses.

## 1.0.0-beta.1 (2020-05-04)

- Initial release. Please see the README and wiki for information on the new design.
